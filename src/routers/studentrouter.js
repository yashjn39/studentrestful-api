const express = require("express");
const router = new express.Router(); // Creating Router
const Student = require("../models/students");
 
// Define the Router

//Post Method
/*  This part consists of Promise Method
    
    router.post("/students", (req, res) => {
    const student =new Student(req.body);
    student.save().then(()=>{
        res.status(201).send(student);
    }).catch((e)=>{
        res.status(400).send("Error : "+e);
    });
}); */


//This part consists of Async Await
router.post("/students", async (req, res) => {
    try {
        const student = new Student(req.body);
        const createStudent = await student.save();
        res.status(201).send(createStudent);
    } catch (e) {
        res.status(400).send("Error : " + e);
    }
});

//Get Method

//All Students
router.get("/students", async (req, res) => {
    try {
        const allStudents = await Student.find();
        res.send(allStudents);;
    } catch (e) {
        res.send(e);
    }
});

//Get Student by Phone
router.get("/students/:phone", async (req, res) => {
    try {
        const _phone = req.params.phone;
        const studentData = await Student.find({ phone: _phone });

        if (!studentData) {
            return res.status(404).send();
        }
        else {
            res.send(studentData);
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

//Update Students by Phone 
router.patch("/students/:phone", async (req, res) => {
    try {
        const _phone = req.params.phone;
        const updateStudentData = await Student.findOneAndUpdate({phone:_phone},req.body ,{
            new : true
        });

        if (!updateStudentData) {
            return res.status(404).send();
        }
        else {
            res.send(updateStudentData);
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

//Delete Student by Phone
router.delete("/students/:phone", async (req, res) => {
    try {
        const _phone = req.params.phone;
        const deleteStudentData = await Student.findOneAndDelete({phone:_phone},{
            new : true
        });

        if (!deleteStudentData) {
            return res.status(404).send();
        }
        else {
            res.send(deleteStudentData);
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports=router;