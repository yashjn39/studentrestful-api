require("./db/conn");
const express = require("express");
const studentRouter = require("./routers/studentrouter");

const app = express();
const port = process.env.PORT || 8000;

app.use(express.json()); 
app.use(studentRouter);  // Register Router

app.listen(port, () => {
    console.log("Express connecton is setup @ Port Number : " + port);
});

