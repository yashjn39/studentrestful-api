const { truncate } = require("fs");
const mongoose = require("mongoose");


mongoose.connect("mongodb://localhost:27017/students-api", {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Mongo DB Connected Succesfully.");
}).catch((e) => {
    console.log("No Connection : " + e);
});

